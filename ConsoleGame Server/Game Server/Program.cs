﻿using System;
using System.Threading;

namespace Game_Server
{
    class Program
    {
        private static bool isRunning = false;
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            isRunning = true;

            Thread _mainThread = new Thread(new ThreadStart(MainThread));
            _mainThread.Start();
            Server.Start(50, 26950);

            
        }

        private static void MainThread()
        {
            Console.WriteLine($"Main thrread started. Running at {Constants.TICKS_PER_SEC} ticks per second");
            DateTime _nextLoop = DateTime.Now;

            while(isRunning)
            {
                while(_nextLoop < DateTime.Now)
                {
                    GameLogic.Update();

                    _nextLoop = _nextLoop.AddMilliseconds(Constants.MS_PER_TICK);

                    if(_nextLoop > DateTime.Now)
                    {
                        Thread.Sleep(_nextLoop - DateTime.Now);
                    }
                }
            }
        }
    }
}
