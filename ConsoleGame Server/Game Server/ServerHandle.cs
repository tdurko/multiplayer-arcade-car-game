﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Game_Server
{
    class ServerHandle
    {
        public static void WelcomeReceived(int _fromClient, Packet packet)
        {
            int _clientIdCheck = packet.ReadInt();
            string _username = packet.ReadString();

            Console.WriteLine($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} connected succesfully and {_username} is now player {_fromClient}.");
            if(_fromClient != _clientIdCheck)
            {
                Console.WriteLine($"Player \"{_username}\" ID: {_fromClient} has assumed the wrong client ID ({_clientIdCheck}!");
            }

            Server.clients[_fromClient].SendIntoGame(_username);
        }

        public static  void PlayerMovement(int _fromClient, Packet packet)
        {
            bool[] _inputs = new bool[packet.ReadInt()];
            for(int i=0;i<_inputs.Length;i++)
            {
                _inputs[i] = packet.ReadBool();
            }
            Quaternion _rotation = packet.ReadQuaterion();

            Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
        }

    }
}
